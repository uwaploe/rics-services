RICS Services
==============

This repository contains the templates for the
[Systemd](http://www.freedesktop.org/wiki/Software/systemd/) configuration
files for the various RICS services.

![Dataflow](dataflow.png "data-flow diagram")

## beaconmon.service

This service monitors the network for Beacon Packets from the RTR nodes.

## datamgr.service

This service manages the routing of data from producers to consumers.

## rtrctl.service

This service sends control messages to the RTR nodes.

## dirsmon.service

This service monitors the serial time-stamp data from DIRS

## gicsmon.service

This service monitors the serial Grid data from GICS. This data is only
needed for converting the RTR GPS locations to grid X-Y.

## ricspub.service

This service subscribes to the messages published by the Data Manager and
republishes them on a [Redis](http://redis.io) pub-sub channel. This
service is not shown directly on the data-flow diagram but it is part of
the user-interface.

## gridcalc.service

This service subscribes to the GRID and GPS messages published by the Data
Manager and uses this information to calculate the grid X-Y locations of
the RTR nodes. This information is sent back to the Data Manager in an
XYPOS message.

## gpstrack.service

This service subscribes to the GRID messages published by the Data Manager
and monitors the X-Y serial output from RADS to convert tracked target X-Y
grid positions to GPS positions.
