#
# Install RICS services
#
SHELL = /bin/bash
BINDIR = $(HOME)/bin
SVCDIR = $(HOME)/.config/systemd/user
CFGDIR = $(HOME)/.config/rics
DATADIR = $(HOME)/data

PROGS := venv_helper.sh startsv.sh stopsv.sh firehose.sh
SVCS := beaconmon.service datamgr.service dirsmon.service \
        ricspub.service rtrctl.service gicsmon.service \
        gridcalc.service gpstrack.service webapp.service

.PHONY: install install-sv install-bin

all: install

$(SVCS): %.service: %.service.in
	sed -e "s!@HOME@!$(HOME)!g" \
        -e "s!@BINDIR@!$(BINDIR)!g" \
        -e "s!@DATADIR@!$(DATADIR)!g" \
        -e "s!@CFGDIR@!$(CFGDIR)!g" $< > $@

install: install-sv install-bin

install-sv: $(SVCS) rics.target
	install -d $(SVCDIR)
	install -m 644 -t $(SVCDIR) $^
	systemctl --user enable $^

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $^

clean:
	rm -f *.service
