#!/bin/bash
#
# Start the RTR network service on all nodes
#

if [[ $# -gt 0 ]]; then
    nodes="$@"
else
    nodes="rtr-1 rtr-2 rtr-3 rtr-4"
fi

for node in $nodes; do
    echo -n "$node ... "
    if ping -c 1 -n -q $node 1> /dev/null 2>&1; then
        echo "online"
        ssh $node 'cd service && sv start ./rtrd'
    else
        echo "offline"
    fi
done
