#!/bin/bash
#
# Display all of the JSON messages published by the Data Manager.
#

# Redis pub/sub channel to monitor
CHAN="rics"

usage() {
    echo "$(basename $1) [msg_type]" 1>&2
}

# We need GNU sed
if which gsed 1> /dev/null 2>&1; then
    SED=gsed
else
    SED=sed
fi

# ... and stdbuf
if which gstdbuf 1> /dev/null 2>&1; then
    STDBUF=gstdbuf
else
    STDBUF=stdbuf
fi

if [[ -z "$1" ]]; then
    $STDBUF -oL redis-cli --raw subscribe "$CHAN" |\
        $SED -u -n '/^message/,+2p' |\
        $SED -u -n '0~3p' |\
        jq --unbuffered '.'
else
    $STDBUF -oL redis-cli --raw subscribe "$CHAN" |\
        $SED -u -n '/^message/,+2p' |\
        $SED -u -n '0~3p' |\
        jq --unbuffered "select(.[0] == \"$1\") | .[1]"
fi
